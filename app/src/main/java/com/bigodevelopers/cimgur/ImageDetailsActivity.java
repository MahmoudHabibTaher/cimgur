package com.bigodevelopers.cimgur;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.bigodevelopers.cimgur.list.model.ImageDataView;

/**
 * Created by mahmoud on 5/17/17.
 */

public class ImageDetailsActivity extends AppCompatActivity {
    private static final String TAG = ImageDetailsActivity.class.getSimpleName();

    private static final String EXTRA_IMAGE_DATA = "EXTRA_IMAGE_DATA";

    public static Intent getStartIntent(@NonNull Context context, @NonNull ImageDataView imageData) {
        Intent intent = new Intent(context, ImageDetailsActivity.class);
        intent.putExtra(EXTRA_IMAGE_DATA, imageData);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_details);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(EXTRA_IMAGE_DATA)) {
            ImageDataView imageDataView = extras.getParcelable(EXTRA_IMAGE_DATA);
            showImagePreviewFragment(imageDataView);
        }
    }

    private void showImagePreviewFragment(ImageDataView imageDataView) {
        ImagePreviewFragment imagePreviewFragment = ImagePreviewFragment.newInstance(imageDataView);
        getSupportFragmentManager().beginTransaction().add(R.id.image_preview_fragment_container, imagePreviewFragment).commit();
    }
}
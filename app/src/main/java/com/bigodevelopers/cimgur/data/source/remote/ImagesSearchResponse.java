package com.bigodevelopers.cimgur.data.source.remote;

import com.bigodevelopers.cimgur.data.model.ImageData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mahmoud on 5/15/17.
 */

public class ImagesSearchResponse {
    @Expose
    @SerializedName("total")
    private Integer total;
    @Expose
    @SerializedName("totalHits")
    private Integer totalHits;
    @Expose
    @SerializedName("hits")
    private List<ImageData> imageDataList;

    public List<ImageData> getImageDataList() {
        return imageDataList;
    }

    public void setImageDataList(List<ImageData> imageDataList) {
        this.imageDataList = imageDataList;
    }
}

package com.bigodevelopers.cimgur.data.model;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mahmoud on 5/14/17.
 */

public class ImageData {
    @Expose
    @SerializedName("id")
    private String id;
    @Expose
    @SerializedName("webformatURL")
    private String imageUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}

package com.bigodevelopers.cimgur.data.source.remote;

import android.support.annotation.NonNull;

import com.bigodevelopers.cimgur.data.model.ImageData;
import com.bigodevelopers.cimgur.data.source.ImagesDataSource;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mahmoud on 5/15/17.
 */

public class RemoteImagesDataSource implements ImagesDataSource {
    private static final String TAG = RemoteImagesDataSource.class.getSimpleName();

    private static final int PER_PAGE_LIMIT = 65;

    private static RemoteImagesDataSource INSTANCE = null;

    public static RemoteImagesDataSource getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new RemoteImagesDataSource();
        }

        return INSTANCE;
    }

    private PixaBayApi mPixaBayApi;

    private RemoteImagesDataSource() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(PixaBayApiHelper.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mPixaBayApi = retrofit.create(PixaBayApi.class);
    }

    @NonNull
    @Override
    public Observable<List<ImageData>> getImagesList() {
        return mPixaBayApi.getImages(PixaBayApiHelper.API_KEY,PER_PAGE_LIMIT).flatMap(galleryResponse -> Observable.just(galleryResponse.getImageDataList()));
    }

    @NonNull
    @Override
    public Observable<ImageData> getImageData(@NonNull String imageHash) {
        return null;
    }
}

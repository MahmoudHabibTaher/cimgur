package com.bigodevelopers.cimgur.data.source.remote;

import android.support.annotation.NonNull;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by mahmoud on 5/15/17.
 */

public interface PixaBayApi {
    @GET("api/")
    Observable<ImagesSearchResponse> getImages(@Query("key") String apiKey, @Query("per_page") Integer perPage);

    @GET("image")
    Observable<ImageResponse> getImage(@NonNull String imageHash);
}

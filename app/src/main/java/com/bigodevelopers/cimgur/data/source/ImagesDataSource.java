package com.bigodevelopers.cimgur.data.source;

import android.support.annotation.NonNull;

import com.bigodevelopers.cimgur.data.model.ImageData;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by mahmoud on 5/14/17.
 */

public interface ImagesDataSource {

    @NonNull
    Observable<List<ImageData>> getImagesList();

    @NonNull
    Observable<ImageData> getImageData(@NonNull String imageHash);
}

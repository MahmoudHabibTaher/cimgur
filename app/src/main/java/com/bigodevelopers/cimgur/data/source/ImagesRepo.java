package com.bigodevelopers.cimgur.data.source;

import android.support.annotation.NonNull;

import com.bigodevelopers.cimgur.data.model.ImageData;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by mahmoud on 5/14/17.
 */

public class ImagesRepo implements ImagesDataSource {
    private static final String TAG = ImagesRepo.class.getSimpleName();

    private static ImagesRepo INSTANCE = null;

    public synchronized static ImagesRepo getInstance(ImagesDataSource localDataSource, ImagesDataSource remoteDataSource) {
        if(INSTANCE == null) {
            INSTANCE = new ImagesRepo(localDataSource, remoteDataSource);
        }

        return INSTANCE;
    }

    private ImagesDataSource mLocalDataSource;
    private ImagesDataSource mRemoteDataSource;
    private boolean mCacheDrity;

    private ImagesRepo(ImagesDataSource localDataSource, ImagesDataSource remoteDataSource) {
        mLocalDataSource = localDataSource;
        mRemoteDataSource = remoteDataSource;
    }

    @NonNull
    @Override
    public Observable<List<ImageData>> getImagesList() {
//        if(mCacheDrity) {
//            return getImagesFromRemote();
//        }
//
//        Observable<List<ImageData>> remoteImages = getImagesFromRemote();

        return getImagesFromRemote();
    }

    @NonNull
    @Override
    public Observable<ImageData> getImageData(@NonNull String imageHash) {
        return getImageFromRemote(imageHash);
    }

    public boolean isCacheDity() {
        return mCacheDrity;
    }

    public void invalidateCache() {
        mCacheDrity = true;
    }

    private Observable<List<ImageData>> getImagesFromRemote() {
        return mRemoteDataSource.getImagesList().doOnNext(imageDatas -> {
            saveToLocalDataSource(imageDatas);
            mCacheDrity = false;
        });
    }

    private void saveToLocalDataSource(List<ImageData> imageDataList) {

    }

    private Observable<ImageData> getImageFromRemote(@NonNull String imageHash) {
        return mRemoteDataSource.getImageData(imageHash);
    }
}

package com.bigodevelopers.cimgur.list.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bigodevelopers.cimgur.R;
import com.bigodevelopers.cimgur.list.model.ImageDataView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.Headers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mahmoud on 5/16/17.
 */

public class ImagesGridAdapter extends RecyclerView.Adapter<ImagesGridAdapter.ViewHolder> {
    private static final String TAG = ImagesGridAdapter.class.getSimpleName();

    private Context mContext;
    private LayoutInflater mInflater;
    private List<ImageDataView> mImages;
    private OnImageClickListener mOnImageClickListener;

    public ImagesGridAdapter(Context context, List<ImageDataView> images) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mImages = images;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_image_grid_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageDataView imageData = mImages.get(position);
        Glide.with(mContext).load(imageData.getImageUrl()).into(holder.mImageView);

        holder.itemView.setOnClickListener(view -> {
            if (mOnImageClickListener != null) {
                mOnImageClickListener.onImageClick(imageData);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    public void setOnImageClickListener(OnImageClickListener listener) {
        mOnImageClickListener = listener;
    }

    public interface OnImageClickListener {
        void onImageClick(@NonNull ImageDataView imageData);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.item_image_view);
        }
    }
}

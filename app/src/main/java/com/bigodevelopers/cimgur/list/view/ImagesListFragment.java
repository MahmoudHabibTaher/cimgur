package com.bigodevelopers.cimgur.list.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bigodevelopers.cimgur.R;
import com.bigodevelopers.cimgur.list.ImagesListContract;
import com.bigodevelopers.cimgur.list.model.ImageDataView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by mahmoud on 5/16/17.
 */

public class ImagesListFragment extends Fragment implements ImagesListContract.View {
    private static final String TAG = ImagesListFragment.class.getSimpleName();

    private ImagesListContract.Presenter mPresenter;

    private View mLoadingView;
    private RecyclerView mImagesRecyclerView;
    private List<ImageDataView> mImageDataList;
    private ImagesGridAdapter mAdapter;

    private OnImageSelectedListener mOnImageSelectedListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_images_list, container, false);
        init(view);
        return view;

    }

    private void init(@NonNull View view) {
        mLoadingView = view.findViewById(R.id.loading_view);
        mImagesRecyclerView = (RecyclerView) view.findViewById(R.id.images_recycler_view);
        mImagesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));

        mImageDataList = new ArrayList<>();
        mAdapter = new ImagesGridAdapter(getContext(), mImageDataList);
        mAdapter.setOnImageClickListener(this::onImageClick);
        mImagesRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(ImagesListContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showLoadingView() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingView() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showImages(@NonNull List<ImageDataView> images) {
        int size = mImageDataList.size();
        mAdapter.notifyItemRangeRemoved(0, size);
        Flowable.fromIterable(images).subscribe(imageDataView -> {
            mImageDataList.add(imageDataView);
            mAdapter.notifyItemInserted(mImageDataList.size() - 1);
        });
    }

    @Override
    public void showNoImagesAvailable() {

    }

    @Override
    public void showErrorLoadingImages(@NonNull String errorMessage) {

    }

    @Override
    public void showImageDetailsUi(@NonNull String imageHash) {

    }

    public void setOniMageSelectedListener(@Nullable OnImageSelectedListener listener) {
        mOnImageSelectedListener = listener;
    }

    private void onImageClick(ImageDataView imageDataView) {
        if (mOnImageSelectedListener != null) {
            mOnImageSelectedListener.onImageSelected(imageDataView);
        }
    }

    public interface OnImageSelectedListener {
        void onImageSelected(@NonNull ImageDataView imageDataView);
    }
}

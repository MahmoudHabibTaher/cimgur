package com.bigodevelopers.cimgur.list;

import android.support.annotation.NonNull;

import com.bigodevelopers.cimgur.common.BasePresenter;
import com.bigodevelopers.cimgur.common.BaseView;
import com.bigodevelopers.cimgur.list.model.ImageDataView;

import java.util.List;

/**
 * Created by mahmoud on 5/16/17.
 */

public interface ImagesListContract {
    interface View extends BaseView<Presenter>{
        void showLoadingView();

        void hideLoadingView();

        void showImages(@NonNull List<ImageDataView> images);

        void showNoImagesAvailable();

        void showErrorLoadingImages(@NonNull String errorMessage);

        void showImageDetailsUi(@NonNull String imageHash);
    }

    interface Presenter extends BasePresenter{
        void loadImagesList();

        void openImageDetails(@NonNull ImageDataView image);
    }
}

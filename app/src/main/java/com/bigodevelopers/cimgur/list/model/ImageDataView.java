package com.bigodevelopers.cimgur.list.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mahmoud on 5/16/17.
 */

public class ImageDataView implements Parcelable {
    private String id;
    private String imageUrl;

    public ImageDataView() {

    }

    protected ImageDataView(Parcel in) {
        id = in.readString();
        imageUrl = in.readString();
    }

    public static final Creator<ImageDataView> CREATOR = new Creator<ImageDataView>() {
        @Override
        public ImageDataView createFromParcel(Parcel in) {
            return new ImageDataView(in);
        }

        @Override
        public ImageDataView[] newArray(int size) {
            return new ImageDataView[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(imageUrl);
    }
}

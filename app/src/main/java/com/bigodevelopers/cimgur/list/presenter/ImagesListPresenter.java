package com.bigodevelopers.cimgur.list.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.bigodevelopers.cimgur.data.model.ImageData;
import com.bigodevelopers.cimgur.data.source.ImagesDataSource;
import com.bigodevelopers.cimgur.list.ImagesListContract;
import com.bigodevelopers.cimgur.list.model.ImageDataView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by mahmoud on 5/16/17.
 */

public class ImagesListPresenter implements ImagesListContract.Presenter {
    private static final String TAG = ImagesListPresenter.class.getSimpleName();
    private ImagesListContract.View mView;
    private ImagesDataSource mImagesDataSource;
    private CompositeDisposable mCompositeDisposable;

    public ImagesListPresenter(ImagesListContract.View view, ImagesDataSource imagesDataSource) {
        mView = view;
        mView.setPresenter(this);

        mImagesDataSource = imagesDataSource;

        mCompositeDisposable = new CompositeDisposable();
    }


    @Override
    public void subscribe() {
        loadImagesList();
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.dispose();
    }

    @Override
    public void loadImagesList() {
        mCompositeDisposable.clear();

        Disposable disposable = mImagesDataSource.getImagesList()
                .map(this::mapToModelView)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(imageDataViewList -> mView.showImages(imageDataViewList)
                        , throwable -> {
                            Log.e(TAG, "Error loading gallery", throwable);
                        });
        mCompositeDisposable.add(disposable);
    }

    @Override
    public void openImageDetails(@NonNull ImageDataView image) {

    }

    private List<ImageDataView> mapToModelView(List<ImageData> imageDataList) {
        List<ImageDataView> imageDataViewList = new ArrayList<>();
        for (ImageData imageData : imageDataList) {
            ImageDataView imageDataView = new ImageDataView();
            imageDataView.setId(imageData.getId());
            imageDataView.setImageUrl(imageData.getImageUrl());
            imageDataViewList.add(imageDataView);
        }
        return imageDataViewList;
    }
}

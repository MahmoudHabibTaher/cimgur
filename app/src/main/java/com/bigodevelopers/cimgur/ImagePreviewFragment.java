package com.bigodevelopers.cimgur;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bigodevelopers.cimgur.list.model.ImageDataView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.cache.MemoryCache;

/**
 * Created by mahmoud on 5/17/17.
 */

public class ImagePreviewFragment extends Fragment {
    private static final String TAG = ImagePreviewFragment.class.getSimpleName();

    private static final String ARG_IMAGE_DATA = "ARG_IMAGE_DATA";

    public static ImagePreviewFragment newInstance(@NonNull ImageDataView imageDataView) {
        ImagePreviewFragment imagePreviewFragment = new ImagePreviewFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_IMAGE_DATA, imageDataView);
        imagePreviewFragment.setArguments(args);
        return imagePreviewFragment;
    }

    private ImageView mPreviewImageView;

    private ImageDataView mImageDataView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(ARG_IMAGE_DATA)) {
                mImageDataView = args.getParcelable(ARG_IMAGE_DATA);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_preview, container, false);
        init(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Glide.with(this).load(mImageDataView.getImageUrl()).into(mPreviewImageView);
    }

    @Override
    public void onDestroyView() {
        Glide.get(getContext()).clearMemory();
        super.onDestroyView();
    }

    private void init(@NonNull View view) {
        mPreviewImageView = (ImageView) view.findViewById(R.id.preview_image_view);
    }
}

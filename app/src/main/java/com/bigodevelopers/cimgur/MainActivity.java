package com.bigodevelopers.cimgur;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.bigodevelopers.cimgur.data.model.ImageData;
import com.bigodevelopers.cimgur.data.source.ImagesRepo;
import com.bigodevelopers.cimgur.data.source.remote.RemoteImagesDataSource;
import com.bigodevelopers.cimgur.list.model.ImageDataView;
import com.bigodevelopers.cimgur.list.presenter.ImagesListPresenter;
import com.bigodevelopers.cimgur.list.view.ImagesListFragment;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImagesListFragment imagesListFragment = (ImagesListFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (imagesListFragment == null) {
            imagesListFragment = new ImagesListFragment();
            imagesListFragment.setOniMageSelectedListener(this::onImageSelected);
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, imagesListFragment, null).commit();
            ImagesListPresenter imagesListPresenter = new ImagesListPresenter(imagesListFragment, ImagesRepo.getInstance(null, RemoteImagesDataSource.getInstance()));
        }
    }

    private void onImageSelected(ImageDataView imageDataView) {
        showImagePreviewFragment(imageDataView);
//        startImageDetailsActivity(imageDataView);
    }

    private void showImagePreviewFragment(ImageDataView imageDataView) {
        ImagePreviewFragment imagePreviewFragment = ImagePreviewFragment.newInstance(imageDataView);
        getSupportFragmentManager().beginTransaction().replace(R.id.image_preview_fragment_container, imagePreviewFragment).commit();
    }

    private void startImageDetailsActivity(ImageDataView imageDataView) {
        startActivity(ImageDetailsActivity.getStartIntent(this, imageDataView));
    }
}

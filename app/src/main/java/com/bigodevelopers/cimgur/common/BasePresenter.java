package com.bigodevelopers.cimgur.common;

/**
 * Created by mahmoud on 5/14/17.
 */

public interface BasePresenter {
    void subscribe();

    void unsubscribe();
}

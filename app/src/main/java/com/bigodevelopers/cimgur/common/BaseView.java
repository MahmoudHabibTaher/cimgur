package com.bigodevelopers.cimgur.common;

/**
 * Created by mahmoud on 5/16/17.
 */

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);
}
